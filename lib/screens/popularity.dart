import 'package:flutter/material.dart';
import 'package:retail/models/location.dart';
class Popularity extends StatelessWidget {
  var  locations =[
    Location(
      place: 'Wayanad', 
      address: 'Shobhika Wedding'),
       Location(
      place: 'Feroke', 
      address: 'Shobhika Wedding'),
       Location(
      place: 'Kunnamangalam', 
      address: 'Shobhika Wedding'),
  ];
  //demo
  
 var images =[
    "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
     "https://cdn.pixabay.com/photo/2016/05/05/02/37/sunset-1373171_960_720.jpg",
     "https://cdn.pixabay.com/photo/2017/02/01/22/02/mountain-landscape-2031539_960_720.jpg"
    
  ];
  ////////
 
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: 
       Container(
               height: MediaQuery.of(context).size.height,
               width: double.infinity,
          child:ListView.builder(
            itemBuilder: (ctx,index){
          
              return Card(
                margin: EdgeInsets.symmetric(vertical:8,horizontal:5),
                child:Column(children: [
                   ListTile(
                     isThreeLine: true,
                leading: CircleAvatar(
                  backgroundImage: NetworkImage(images[index],),
                  radius: 30.0,
                    ),
               
                
              title: Text(locations[index].place,style: TextStyle(fontSize:15.0,fontWeight:FontWeight.bold),),
              subtitle: Column(
                crossAxisAlignment:CrossAxisAlignment.stretch,

                children:[
              Text(locations[index].address,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
             // Text(retails[index].email,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
             // Text(retails[index].phoneno,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
              ]),
            
          
              
           trailing:
            RaisedButton.icon(

                  onPressed: (){},
                       shape: RoundedRectangleBorder(
                         borderRadius:BorderRadius.all(Radius.circular(10.0))
                       ),
                       
                       icon: Icon(Icons.perm_phone_msg,color: Colors.blueAccent,), 
                       label: Text('Call Now',style:TextStyle(fontSize: 10)),
                       textColor: Colors.blueAccent,
                       ),
                   ),
                    ]),   );
            },
            itemCount: locations.length,
         
           ),
    ),
     
    );
      
    
  }
}

                       
    
  