import 'package:flutter/material.dart';
import './popularity.dart';
import './location.dart';
class TabsScreen extends StatefulWidget {
 

  @override
  _TabsScreenState createState() => _TabsScreenState();
}

class _TabsScreenState extends State<TabsScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2, 
    child: Scaffold(
      appBar:AppBar(
        title:Text('Retail'),
        bottom: TabBar(tabs: <Widget>[
          Tab(icon: Icon(Icons.power_input),
          text: 'Popularity',),
          Tab(icon:Icon(Icons.location_on),
          text:'Location'),
        ]),
      ),
      body:TabBarView(
        children:<Widget>[ 
         
        
          Location(),
           Popularity(),

        ],),
    ),
    );
  }
}