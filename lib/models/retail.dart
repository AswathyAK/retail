import 'package:flutter/foundation.dart';

class Retail{
  final String title;
  final String subtitle;
  final String rating;
  final String email;
  final String phoneno;
  Retail({
    @required this.title,
    @required this.subtitle,
    @required this.rating,
    @required this.email,
    @required this.phoneno,
  });
}