import 'package:flutter/widgets.dart';

class Location{
  final String place;
  final String address;

  Location({
     @required this.place, 
     @required this.address
     });
  
}