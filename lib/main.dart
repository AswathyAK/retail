

import 'package:flutter/material.dart';
//import 'package:five_star_rating/five_star_rating.dart';
//import 'package:retail/screens/popularity.dart';
//import './models/retail.dart';
//import 'screens/location.dart';
import './screens/popularity.dart';
import './screens/tabs_screen.dart';

 
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Flutter Demo',
      theme: ThemeData(
        
        primarySwatch: Colors.blue,
        
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
     // home: MyHomePage(),

      initialRoute: '/',
      routes: {
       // '/':(ctx) => CategoriesScreen(),
         '/':(ctx) => TabsScreen(),
        // '/category_meals':(ctx) => CategoryMealsScreen()
       // Location.routeName:(ctx) => Location(),
      //  Popularity.routeName:(ctx) => Popularity(),
        },
        // onGenerateRoute: (settings){
        //   print(settings.arguments);
        //  // return MaterialPageRoute(builder:(ctx)=>CategoriesScreen(),);
        // },
        // onUnknownRoute: (settings) {
        //   return MaterialPageRoute(builder: (ctx)=> Popularity(),);
        // },
    );
  }
}

// class MyHomePage extends StatefulWidget {

//   @override
//   _MyHomePageState createState() => _MyHomePageState();
// }

// class _MyHomePageState extends State<MyHomePage> {
  
//  final List<Retail> retails = [
//   Retail(title: 'Shobhika Wedding', subtitle: 'Mavoor Road Pottammal ',rating:'3.7',email:'shobhika@gmail.com',phoneno:'1234569870'),
//   Retail(title: 'Preethi Silks', subtitle: 'Feroke',rating: '3.5',email:'preethi@gmail.com',phoneno:'9874561235'),
//   Retail(title: 'Minsara Silks ', subtitle: 'Kunnamangalam',rating: '3.7',email:'minsara@gmail.com',phoneno:'5624789318')
// ];


//   final List images =[
//     "https://cdn.pixabay.com/photo/2015/04/23/22/00/tree-736885_960_720.jpg",
//      "https://cdn.pixabay.com/photo/2016/05/05/02/37/sunset-1373171_960_720.jpg",
//      "https://cdn.pixabay.com/photo/2017/02/01/22/02/mountain-landscape-2031539_960_720.jpg"
    
//   ];
 
//  double intailrating;
//   @override
//   Widget build(BuildContext context) {
//      return Scaffold(
                
//           appBar: AppBar(
//             title:Text('Retails',style:TextStyle(fontSize: 20.0))
//           ),
//           body:
//              Container(
//                height: MediaQuery.of(context).size.height,
//                width: double.infinity,
//           child:ListView.builder(
//             itemBuilder: (ctx,index){
          
//               return Card(
//                 margin: EdgeInsets.symmetric(vertical:8,horizontal:5),
//                 child:Column(children: [
//                    ListTile(
//                      isThreeLine: true,
//                 leading: CircleAvatar(
//                   backgroundImage: NetworkImage(images[index],),
//                   radius: 30.0,
//                     ),
//                 //   child:Padding(
//                 //     padding: EdgeInsets.all(6),
//                 //     child:FittedBox(
//                 //    // child:Image.asset('assets/images/download.png',fit:BoxFit.cover),),
//                 //   child: Image.network(images[index],fit:BoxFit.fill),),
                   
//                 //     //child:Image.asset(images[index].img),
              
//                 //  ), ),
                
//               title: Text(retails[index].title,style: TextStyle(fontSize:15.0,fontWeight:FontWeight.bold),),
//               subtitle: Column(
//                 crossAxisAlignment:CrossAxisAlignment.stretch,

//                 children:[
//               Text(retails[index].subtitle,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
//               Text(retails[index].email,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
//               Text(retails[index].phoneno,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
//               ]),
            
          
              
//            trailing:
//             RaisedButton.icon(

//                   onPressed: (){},
//                        shape: RoundedRectangleBorder(
//                          borderRadius:BorderRadius.all(Radius.circular(10.0))
//                        ),
                       
//                        icon: Icon(Icons.perm_phone_msg,color: Colors.blueAccent,), 
//                        label: Text('Call Now',style:TextStyle(fontSize: 10)),
//                        textColor: Colors.blueAccent,
//                        ),
           

//                        ),
//               //           Row(
//               //             crossAxisAlignment: CrossAxisAlignment.start,
//               //             mainAxisAlignment: MainAxisAlignment.start,
//               //   children:<Widget>[
//               //      Text(retails[index].title,style: TextStyle(fontSize:15.0,fontWeight:FontWeight.bold),),
                 

//               //      Text(retails[index].subtitle,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
                    
//               //      Text(retails[index].email,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
                   
//               //      Text(retails[index].phoneno,style: TextStyle(fontSize:10.0,fontWeight:FontWeight.normal),),
//               //   ],
//               // ),
//                        Padding(padding: EdgeInsets.only(left: 80,top:2.0),
//                       //  Row(
//                       //    crossAxisAlignment: CrossAxisAlignment.center,
//                       //    mainAxisAlignment: MainAxisAlignment.start,
//                       //    children: [
//                     child:    FiveStarRating(
//           allowHalfRating: true,
//           onRatingChanged: (v) {
//               intailrating = v;
           
//             setState(() {
//                 print(intailrating);
//             });
//           },
//           //starCount: 5,
//           intialrating: 3,
       
//           size: 20.0,
//           filledIconData: Icons.star,
//           halfFilledIconData: Icons.star_half,
//           color: Colors.amber,
//           borderColor: Colors.black,
//           textColor: Colors.black,
//           spacing:0.0
//         ),),
//                       //  ListTile(
//                       //   // title: Text('Rating',style: TextStyle(fontSize:8.0),),
                         
//                       //    leading:Icon(
//                       //      Icons.star,
//                       //     //  Icons.star,
//                       //    color:Colors.amber,),
//                       //  ),

//                  // StarDisplay(),
//                  // StarDisplayWidget(filledStar:null , unfilledStar: null)
//                    ]),   );
//             },
//             itemCount: retails.length,
         
//            ),
//     ),
      
//     );
//   }
// }
